const Equipo = require('../models/equipo.model.js');

// Crea y guarda una Equipo
exports.create = (req, res) => {
    // Validar petición
    if (Object.keys(req.body).length == 0) {
        return res.status(400).send({
            message: "No se puede añadir un registro vacío"
        });
    }

    // Crea una Equipo
    const equipo = new Equipo({
        id: req.body.id,
        tipo: req.body.tipo,
        marca: req.body.marca,
        modelo: req.body.modelo,
        memoria: req.body.memoria,
        discoDuro: req.body.discoDuro,
        procesador: req.body.procesador,
        numeroSerie: req.body.numeroSerie,
        observaciones: req.body.observaciones,
        software: req.body.software,
        estado: req.body.estado
    });

    // Guarda una Equipo en la base de datos
    equipo.save(err => {
        if (err) return res.status(500).send({
            message: err.message || "Ha ocurrido un error en la creación de un Equipo."
        });
        return res.status(200).send(equipo);
    });
};

// Devuelve una lista con todos los equipos de la base de datos
exports.findAll = (req, res) => {

    Equipo.find((err, equipo) => {
        if (err) return res.status(500).send({
            message: err.message || "Ha ocurrido un error al obtener la lista de Equipos."
        });
        return res.status(200).send(equipo);
    });
};

// Obtiene una Equipo dado un identificador
exports.findByEtiqueta = (req, res) => {

    Equipo.find({ 'id': req.params.id }, (err, equipo) => {
        if (err) return res.status(404).send({
            message: "No se ha encontrado el Equipo con identificador " + req.params.id
        });

        return res.status(200).send(equipo);
    });
};

// Modificación de una Equipo con un Id
exports.updateByEtiqueta = (req, res) => {
    // Validar petición
    if (Object.keys(req.body).length == 0) {
        return res.status(400).send({
            message: "El contenido no puede estar vacío"
        });
    }
    var query = { 'id': req.params.id };
    Equipo.findOneAndUpdate(query, {
        id: req.body.id,
        tipo: req.body.tipo,
        marca: req.body.marca,
        modelo: req.body.modelo,
        memoria: req.body.memoria,
        discoDuro: req.body.discoDuro,
        procesador: req.body.procesador,
        numeroSerie: req.body.numeroSerie,
        observaciones: req.body.observaciones,
        software: req.body.software,
        estado: req.body.estado
    }, (err, equipo) => {
        if (err) return res.status(404).send({
            message: "No se ha encontrado el Equipo con nombre " + req.params.id
        });
        return res.send(equipo)
    });
};

// Elimina a una Equipo con un identificador
exports.deleteByEtiqueta = (req, res) => {

    var query = { 'id': req.params.id };
    Equipo.findOneAndDelete(query, (err, equipo) => {
        if (err) return res.status(500).send({
            message: "No se ha podido eliminar el Equipo con nombre " + req.params.id
        });
        return res.status(200).send(equipo);
    });
};