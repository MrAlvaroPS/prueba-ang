const Log = require('../models/log.model.js');

// Crea y guarda una persona
exports.create = (req, res) => {
    // Validar petición
    if (Object.keys(req.body).length == 0) {
        return res.status(400).send({
            message: "No se puede añadir un registro vacío"
        });
    }

    // Crea una persona
    const log = new Log({
        timestamp: req.body.timestamp,
        id_equipo: req.body.id_equipo,
        persona: req.body.persona,
        texto: req.body.texto
    });

    // Guarda un log en la base de datos
    log.save(err => {
        if (err) return res.status(500).send({
            message: err.message || "Ha ocurrido un error en la creación de un log."
        });
        return res.status(200).send(log);
    });
};

// Devuelve una lista con todas los logs de la base de datos
exports.findAll = (req, res) => {

    Log.find((err, log) => {
        if (err) return res.status(500).send({
            message: err.message || "Ha ocurrido un error al obtener la lista de Logs."
        });
        return res.status(200).send(log);
    });
};

// Obtiene un log dado un timestamp
exports.findByTimestamp = (req, res) => {

    Log.find({ 'timestamp': req.params.timestamp }, (err, log) => {
        if (err) return res.status(404).send({
            message: "No se ha encontrado el log con fecha " + req.params.timestamp
        });

        return res.status(200).send(log);
    });
};

// Obtiene un log dado un identificador de equipo
exports.findByIdEquipo = (req, res) => {

    Log.find({ 'id_equipo': req.params.id_equipo }, (err, log) => {
        if (err) return res.status(404).send({
            message: "No se ha encontrado el log del equipo " + req.params.id_equipo
        });

        return res.status(200).send(log);
    });
};

// Obtiene un log dado una persona
exports.findByPersona = (req, res) => {

    Log.find({ 'persona': req.params.persona }, (err, log) => {
        if (err) return res.status(404).send({
            message: "No se ha encontrado el log de la persona " + req.params.persona
        });

        return res.status(200).send(log);
    });
};