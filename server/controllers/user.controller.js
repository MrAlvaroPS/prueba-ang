const User = require('../models/user.model.js');

// Crea y guarda un usuario
exports.create = (req, res) => {
    // Validar petición
    if (Object.keys(req.body).length == 0) {
        return res.status(400).send({
            message: "No se puede añadir un registro vacío"
        });
    }

    // Crea un usuario
    const usuario = new User({
        id: req.body.id,
        password: req.body.password,
        log: req.body.log,
        super: req.body.super,
    });

    // Guarda un usuario en la base de datos
    usuario.save(err => {
        if (err) return res.status(500).send({
            message: err.message || "Ha ocurrido un error en la creación de un usuario."
        });
        return res.status(200).send(usuario);
    });
};

// Obtiene un usuario dado un identificador
exports.findByName = (req, res) => {

    User.find({ 'id': req.params.id }, (err, usuario) => {
        if (err) return res.status(404).send({
            message: "No existe el usuario con nombre " + req.params.id
        });
        return res.status(200).send(usuario);
    });
};

// Elimina a un usuario con un identificador
exports.deleteByName = (req, res) => {

    var query = { 'id': req.params.id };
    User.findOneAndDelete(query, (err, usuario) => {
        if (err) return res.status(500).send({
            message: "No se ha podido eliminar el usuario con nombre " + req.params.id
        });
        return res.status(200).send(usuario);
    });
};