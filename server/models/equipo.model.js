const mongoose = require('mongoose');

const equipoEsquema = mongoose.Schema({
    id: String,
    tipo: String,
    marca: String,
    modelo: String,
    memoria: {
        type: String,
        required: function () {
            return this.tipo === "pantalla" ? false : true;
        }
    },
    discoDuro: {
        type: String,
        required: function () {
            return this.tipo === "pantalla" ? false : true;
        },
        tipo: String,
        capacidad: String
    },
    procesador: {
        type: String,
        required: function () {
            return this.tipo === "pantalla" ? false : true;
        }
    },
    numeroSerie: String,
    observaciones: {
        type: String,
        required: false
    },
    software: {
        type: String,
        required: function () {
            return this.tipo === "pantalla" ? false : true;
        },
        SSOO: String,
        Antivirus: String
    },
    estado: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Equipo', equipoEsquema);