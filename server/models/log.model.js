const mongoose = require('mongoose');

const logEsquema = mongoose.Schema({
    timestamps: Date,
    id_equipo: {
        type: String,
        required: function() {
            return this.persona ? false : true;
        }
    },       
    persona: {
        type: String,
        required: function() {
            return this.id_equipo ? false : true;
        }
    },
    texto: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('Log', logEsquema);