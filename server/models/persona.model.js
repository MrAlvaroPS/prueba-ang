const mongoose = require('mongoose');

const personaEsquema = mongoose.Schema({
    id: String,
    nombreEquipo: String,
    equipo: Array,
    pantalla: {
        type: String,
        required: false
    },
    estado: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('Persona', personaEsquema);