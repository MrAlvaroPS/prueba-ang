module.exports = (app) => {
    const equipos = require('../controllers/equipo.controller.js');

    // Crear un nuevo equipo
    app.post('/equipos', equipos.create);

    // Listar todos los equipos
    app.get('/equipos', equipos.findAll);

    // Obtener un equipo por su id
    app.get('/equipos/:id', equipos.findByEtiqueta);

    // Modificar un equipo por su id
    app.post('/equipos/:id', equipos.updateByEtiqueta);

    // Eliminar un equipo por su id
    app.delete('/equipos/:id', equipos.deleteByEtiqueta);
}