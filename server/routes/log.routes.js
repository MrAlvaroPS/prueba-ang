module.exports = (app) => {
    const logs = require('../controllers/log.controller.js');

    // Crear un nuevo log
    app.post('/logs', logs.create);

    // Listar todos los logs
    app.get('/logs', logs.findAll);

    // Obtener un log por su timestamp
    app.get('/logs/:timestamp', logs.findByTimestamp);

    // Obtener un log por su id de equipo
    app.get('/logs/id_equipo/:id_equipo', logs.findByIdEquipo);

    // Obtener un log por persona
    app.get('/logs/persona/:persona', logs.findByPersona);
}