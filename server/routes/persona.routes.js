module.exports = (app) => {
    const personas = require('../controllers/persona.controller.js');

    // Crear una nueva persona
    app.post('/personas', personas.create);

    // Listar todas las personas
    app.get('/personas', personas.findAll);

    // Obtener una persona por su id
    app.get('/personas/:id', personas.findByName);

    // Update a Note with noteId
    app.post('/personas/:id', personas.updateByName);

    // Eliminar una persona por su id
    app.delete('/personas/:id', personas.deleteByName);
}