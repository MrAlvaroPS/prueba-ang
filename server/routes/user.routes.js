module.exports = (app) => {
    const users = require('../controllers/user.controller.js');

    // Crear un nuevo usuario
    app.post('/users', users.create);

    // Obtener un usuario por su id
    app.get('/users/:id', users.findByName);

    // Eliminar un equipo por su id
    app.delete('/users/:id', users.deleteByName);
}