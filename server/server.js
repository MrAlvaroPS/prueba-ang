const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const PORT = 8080;
const api = require('./routes/api');
const app = express();

require('./routes/equipo.routes');
require('./routes/log.routes');
require('./routes/persona.routes');
require('./routes/user.routes');

app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('/api', api); // 

app.get('/', function (req, res, next) {
    res.send('hello from server');
});

app.listen(PORT, function () {
    console.log('server is running on nicely');
});